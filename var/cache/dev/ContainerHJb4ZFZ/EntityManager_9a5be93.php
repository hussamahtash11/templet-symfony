<?php

namespace ContainerHJb4ZFZ;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder60133 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerafe7b = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties01842 = [
        
    ];

    public function getConnection()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'getConnection', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'getMetadataFactory', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'getExpressionBuilder', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'beginTransaction', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'getCache', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->getCache();
    }

    public function transactional($func)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'transactional', array('func' => $func), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'wrapInTransaction', array('func' => $func), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'commit', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->commit();
    }

    public function rollback()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'rollback', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'getClassMetadata', array('className' => $className), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'createQuery', array('dql' => $dql), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'createNamedQuery', array('name' => $name), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'createQueryBuilder', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'flush', array('entity' => $entity), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'clear', array('entityName' => $entityName), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->clear($entityName);
    }

    public function close()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'close', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->close();
    }

    public function persist($entity)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'persist', array('entity' => $entity), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'remove', array('entity' => $entity), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'refresh', array('entity' => $entity), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'detach', array('entity' => $entity), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'merge', array('entity' => $entity), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'getRepository', array('entityName' => $entityName), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'contains', array('entity' => $entity), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'getEventManager', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'getConfiguration', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'isOpen', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'getUnitOfWork', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'getProxyFactory', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'initializeObject', array('obj' => $obj), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'getFilters', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'isFiltersStateClean', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'hasFilters', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return $this->valueHolder60133->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerafe7b = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder60133) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder60133 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder60133->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, '__get', ['name' => $name], $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        if (isset(self::$publicProperties01842[$name])) {
            return $this->valueHolder60133->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder60133;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder60133;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder60133;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder60133;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, '__isset', array('name' => $name), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder60133;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder60133;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, '__unset', array('name' => $name), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder60133;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder60133;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, '__clone', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        $this->valueHolder60133 = clone $this->valueHolder60133;
    }

    public function __sleep()
    {
        $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, '__sleep', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;

        return array('valueHolder60133');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerafe7b = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerafe7b;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerafe7b && ($this->initializerafe7b->__invoke($valueHolder60133, $this, 'initializeProxy', array(), $this->initializerafe7b) || 1) && $this->valueHolder60133 = $valueHolder60133;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder60133;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder60133;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
