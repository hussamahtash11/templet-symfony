# 1 symfony new my_project_name --webapp

# Cd my_project_name

# php -S localhost:2000 -t public/ or symfony server:start

# npm install

# npm run watch

# 2 create Controller

# php bin/console make:controller

# ... enter your controller nameController

# 3 create database

# enter in .env file

# comment ligne 31
# create a database

# decomment ligne 30 and modify it

# DATABASE_URL="mysql://db_username:db_password@127.0.0.1:3306/   db_nameserverVersion=5.7&charset=utf8mb4"

# php bin/console make:entity

# php bin/console make:migration

# php bin/console doctrine:migrations:migrate // for push to phpmyadmin
`